Project details:
BDD framework developed using maven, java, cucumber, selenium

Pre-req:

To run the project Java and Maven needs to be installed on target machine

Clone project:

git clone https://mrinmoymena@bitbucket.org/mrinmoymena/bddopenweathermap.git

Running test:

cd mybddproject
mvn test (defualt will run on local firefox and chrome browser)

target/cucumber-reports/report.html (Test result report location)

Configuration:

cd 	src/test/java/features
open testcases.feature
User can change data writtent within ""
User can choose browser by changing borwsername within ""

Integration with CI:

Can easily integrated with CI as test case can be run from commandline using command mvn test