package seleniumcode;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import org.junit.AfterClass;
import org.junit.Assert;

import page.*;

public class Selenium {

	public static WebDriver driver;
	public WeatherPage weatherPageObj;

	@Given("^user navigate to homepage$")
	public void user_navigate_to_homepage() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
	}

	@When("^user enters url \"([^\"]*)\" on \"([^\"]*)\"$")
	public void user_is_on_homepage(String url, String browser) throws Throwable {
		if(browser.equals("firefox"))
		{
			System.setProperty("webdriver.gecko.driver","src/test/resources/geckodriver.exe");
			driver = new FirefoxDriver();
		}
		else if(browser.equals("chrome"))
		{
			System.setProperty("webdriver.chrome.driver","src/test/resources/chromedriver.exe");
			driver = new ChromeDriver();
		}
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.get(url);
	}


	@Then("^all important informations are displayed$")
	public void important_message_is_displayed() throws Throwable {
		//String exp_message = "Welcome to your account. Here you can manage all   of your personal information and orders.";
		weatherPageObj = PageFactory.initElements(driver, WeatherPage.class);
		Assert.assertEquals(weatherPageObj.signinlink.getText(),"Sign In");
		Assert.assertEquals(weatherPageObj.signuplink.getText(),"Sign Up");
		Assert.assertEquals(weatherPageObj.weatherlink.getText(),"Weather in your city");
		Assert.assertEquals(weatherPageObj.weatherlabel.getText(),"Current weather and forecasts in your city");
		//driver.quit();  
	}   

	@When("user to enter city name")
	public void user_to_enter_city() throws Throwable { 
		weatherPageObj = PageFactory.initElements(driver, WeatherPage.class);
		//Explicit wait
		WebDriverWait wait = new WebDriverWait(driver, 20); //here, wait time is 20 seconds
		wait.until(ExpectedConditions.visibilityOf(weatherPageObj.searchButton));
		weatherPageObj.searchButton.click();
	}
	@When("^user enters \"([^\"]*)\" and search$")
	public void user_search_invalid(String city) throws Throwable { 
		weatherPageObj = PageFactory.initElements(driver, WeatherPage.class);
		WebDriverWait wait = new WebDriverWait(driver, 20); //here, wait time is 20 seconds
		wait.until(ExpectedConditions.visibilityOf(weatherPageObj.cityTextBox));
		weatherPageObj.cityTextBox.sendKeys(city);
		weatherPageObj.searchButtonNext.click();
	}


	@Then("^message is displayed$")
	public void error_displayed() throws Throwable {
		weatherPageObj = PageFactory.initElements(driver, WeatherPage.class);
		String alertMessage= weatherPageObj.alertError.getText();
		// Displaying alert message		
		Assert.assertTrue(alertMessage.contains("Not found"));
		//driver.quit();  
	}   

	@Given("user to enter valid city name")
	public void user_to_enter_valid_city() throws Throwable { 
		weatherPageObj = PageFactory.initElements(driver, WeatherPage.class);
		weatherPageObj.cityTextBox.clear();
	}
	

	@Then("^weather information is displayed$")
	public void info_displayed() throws Throwable {
		weatherPageObj = PageFactory.initElements(driver, WeatherPage.class);
		String message= weatherPageObj.weatherTable.getText();	
		Assert.assertTrue(message.contains("Pune, IN"));
		Assert.assertTrue(message.contains("Geo coords [18.5203, 73.8543]"));
		//driver.quit();  
	} 
	
	@When("^user enters invalid username \"([^\"]*)\" password \"([^\"]*)\"$")
	public void user_enters_invalid_username(String user, String pass) throws Throwable {
		weatherPageObj = PageFactory.initElements(driver, WeatherPage.class);
		weatherPageObj.signinlink.click();
		WebDriverWait wait = new WebDriverWait(driver, 20); //here, wait time is 20 seconds
		wait.until(ExpectedConditions.visibilityOf(weatherPageObj.emailText));
		weatherPageObj.emailText.clear();
		weatherPageObj.emailText.sendKeys(user);
		weatherPageObj.passwordText.clear();
		weatherPageObj.passwordText.sendKeys(pass);
		weatherPageObj.submitButton.click();
		
	}

	@Then("^login error message displayed$")
	public void login_error_message_displayed() throws Throwable {
		weatherPageObj = PageFactory.initElements(driver, WeatherPage.class);
		Assert.assertEquals("Invalid Email or password.", weatherPageObj.loginError.getText());
	   
	}
	
	@After
    public static void tearDown() {
        System.out.println("tearing down");
        driver.quit();
    }
}
