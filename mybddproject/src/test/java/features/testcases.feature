#Author: mrinmoymena1@gmail.com
#Keywords Summary :
@testing
Feature: Starts at https://openweathermap.org/
  Verifies that all important information is there

  Scenario: User navigate to https://openweathermap.org/
    Given user navigate to homepage 
    When user enters url "https://openweathermap.org/" on "chrome"
    Then all important informations are displayed
  Scenario: Enter invalid city
    Given user navigate to homepage
    When user enters url "https://openweathermap.org/" on "chrome"
    And user to enter city name
    And user enters "ddd" and search
    Then message is displayed
  Scenario: Enter valid city
    Given user navigate to homepage
    When user enters url "https://openweathermap.org/" on "firefox"
    And user to enter city name
    And user enters "pune" and search
    Then weather information is displayed
  Scenario: Login into homepage
    Given user navigate to homepage
    When user enters url "https://openweathermap.org/" on "chrome"
    And user enters invalid username "user1@gmail.com" password "pass1"
    Then login error message displayed
