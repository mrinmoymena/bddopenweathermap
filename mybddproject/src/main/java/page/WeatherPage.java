package page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

public class WeatherPage {

	@FindBy(linkText= "Sign In" )
	public WebElement signinlink;
	@FindBy(linkText= "Sign Up" )
	public WebElement signuplink;
	@FindBy(linkText= "Weather in your city" )
	public WebElement weatherlink;
	@FindBy(css= "#widget>div> div>h2" )
	public WebElement weatherlabel;
	@FindBy(id= "search_str" )
	public WebElement cityTextBox;
	@FindBy(css= "button.btn-orange" )
	public WebElement searchButton;
	@FindBy(css= "button.btn-color" )
	public WebElement searchButtonNext;
	@FindBy(css= "#forecast_list_ul>div" )
	public WebElement alertError;
	@FindBy(css="#forecast_list_ul>table")
	public WebElement weatherTable;
	@FindBy(id="user_email")
	public WebElement emailText;
	@FindBy(id="user_password")
	public WebElement passwordText;
	@FindBy(css="input.btn-color[value=Submit]")
	public WebElement submitButton;
	@FindBy(css="body > div.wrapper > div:nth-child(3) > div > div > div > div.panel-body")
	public WebElement loginError;

}
